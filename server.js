// Load the TCP Library
const net = require('net');
const port = process.env.PORT || 8080;
// Keep track of the chat clients
var clients = [];

// Start a TCP Server
let server = net.createServer(function (socket) {

  // Identify this client
  socket.name = socket.remoteAddress + ":" + socket.remotePort

  // Put this new client in the list
  clients.push(socket);

  // Send a nice welcome message and announce
  socket.write("Welcome " + socket.name + "\n");
  broadcast(socket.name + " joined the chat\n", socket);
  checkConnection();
  socket.on('error', function(err){
    //console.log(err);
    clients.splice(clients.indexOf(socket), 1);
    broadcast(socket.name + " disconnect\n");
  });
  // Handle incoming messages from clients.
  socket.on('data', function (req) {
    data = req.toString();
    broadcast(socket.name + "> " + data, socket);
  });

  // Remove the client from the list when it leaves
  socket.on('end', function () {
    clients.splice(clients.indexOf(socket), 1);
    broadcast(socket.name + " disconnect\n");
  });

  // Send a message to all clients
  function broadcast(message, sender) {
    clients.forEach(function (client) {
      // Don't want to send it to sender
      if (client === sender) return;
      client.write(message);
    });
    // Log it to the server output too
    process.stdout.write(message)
  }

  function checkConnection(){
    if(true){
      setTimeout(function () {
        clients.forEach(function (client) {
            client.write("msg:ping\n");
        });
        checkConnection();
      }, 1000);
    }
  }

}).listen(port);
